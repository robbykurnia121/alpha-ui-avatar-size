const plugin = require('tailwindcss/plugin');

const avatarSize = plugin(
  function ({ addUtilities, theme, variants, e }) {
    const values = theme('avatarSize');

    addUtilities(
      [
        Object.entries(values).map(([key, value]) => {
          return {
            [`.${e(`avatar-size-${key}`)}`]: {
              'width': `${value}`,
              'height': `${value}`,
            },
          };
        }),
      ],
      variants('avatarSize')
    );
  },
  {
    theme: {
      avatarSize: {
        'xs': '1.25rem',
        'sm': '2.5rem',
        'md': '4.125rem',
        'lg': '5rem',
        'xl': '5.25rem',
        '2xl': '6.25rem',
      },
    },
    variants: {
      avatarSize: [''],
    },
  }
);

module.exports = avatarSize;